var inRow = document.getElementById('row');
var error = document.getElementById('error');
var inCol = document.getElementById('col');
var btnGenerate = document.getElementById('btnGen');
var table =  document.createElement('table');

btnGenerate.onclick = function(e){
    var inRowVal = inRow.value;
    var inColVal = inCol.value;

    

    if(inRowVal === '' && inColVal === ''){
        error.style.display = 'block';
        error.textContent = 'Invalid'
        error.style.setProperty('color','red')
    }else{
        
        error.style.display = 'none';
        genTable(inRowVal,inColVal);
        for(var r = 0, rt;rt = table.rows[r];r++){
            for(var k =0, ct; ct = rt.cells[k];k++){

                table.rows[r].cells[k].firstChild.onblur = validationBlur

                if(ct.firstElementChild.id !== ''){
                    validation(ct.firstElementChild,rt);
                }
            }            
        }
    }

}

function validation(ele,parent){
    ele.onkeypress = function(e){
        if(e.keyCode == 13){
            parent.childNodes.forEach(element => {
                var childTemp  = element.firstElementChild
                if(childTemp.value === ''){
                    
                    alert("Invalid");
                    
                }
            });
        }
    }
}

function validationBlur(e){
    if(e.target.value === ""){
        // console.log("Invalid")
        alert('Invalid');
        console.log("Col: "+(e.path[1].cellIndex + 1) +"- Row: "+ (e.path[1].parentNode.rowIndex + 1));
        return;
      
    }
}



function genTable(row,col){
    
    table.style.setProperty('margin-left','20px');

    for(var i = 0; i < row;i++){
        var tr = document.createElement('tr');
       
       for(var j = 0; j< col;j++){
            var td = document.createElement('td');
            var inputNode = document.createElement('input');
            inputNode.type = 'text';

            if(j == col - 1){
                inputNode.id = i+""+j;
            }

            td.appendChild(inputNode);
            tr.appendChild(td);
       }
        
        table.appendChild(tr);
    }
    document.body.appendChild(table);
}

